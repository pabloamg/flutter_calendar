import 'package:flutter/material.dart';
import 'package:date_utils/date_utils.dart';

class CalendarTile extends StatelessWidget {
  final VoidCallback onDateSelected;
  final DateTime date;
  final String dayOfWeek;
  final bool isDayOfWeek;
  final bool isSelected;
  final TextStyle dayOfWeekStyles;
  final TextStyle dateStyles;
  final TextStyle selectedStyle;
  final Widget child;
  final BoxDecoration decoration;
  final BoxDecoration selectedDecoration;

  CalendarTile({
    this.onDateSelected,
    this.date,
    this.dateStyles,
    this.dayOfWeek,
    this.dayOfWeekStyles,
    this.isDayOfWeek: false,
    this.isSelected: false,
    this.selectedStyle,
    this.child,
    this.decoration,
    this.selectedDecoration,
  });

  Widget renderDateOrDayOfWeek(BuildContext context) {
    if (isDayOfWeek) {
      return new InkWell(
        child: new Container(
          alignment: Alignment.center,
          child: new Text(
            dayOfWeek,
            style: dayOfWeekStyles,
          ),
        ),
      );
    } else {
      return new InkWell(
        onTap: onDateSelected,
        child: new Container(
          decoration: isSelected
              ? (selectedDecoration ??
                  new BoxDecoration(
                    shape: BoxShape.circle,
                    color: Theme.of(context).primaryColor,
                  ))
              : (decoration ?? BoxDecoration(color: Colors.white)),
          alignment: Alignment.center,
          child: new Text(
            Utils.formatDay(date).toString(),
            style: isSelected
                ? (selectedStyle ?? TextStyle(color: Colors.white))
                : dateStyles,
            textAlign: TextAlign.center,
          ),
        ),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    if (child != null) {
      return new InkWell(
        child: child,
        onTap: onDateSelected,
      );
    }
    return new Container(
      padding: EdgeInsets.all(1.0),
      decoration: decoration ?? BoxDecoration(color: Colors.white),
      child: renderDateOrDayOfWeek(context),
    );
  }
}
